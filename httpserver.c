#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#define MAXLINE 10000
#define SERV_TCP_PORT 5566
#define MAX_CMDNUMBER 4000
#define PATHSIZE 1024
#define BUFFSIZE 4096
#define ACK_MSG "HTTP/1.1 200 OK\r\n"
#define CONTENT_TYPE "Content-Type: text/html\n\n"

int herror(char* p){
	perror(p);
	exit(1);
}

int cutline(char *line, char *cmd[], char *cut){
	if(!line) return 0;
	char *str = NULL;
	char *ptr = NULL;
	char *dup = strdup(line);

	str = strtok_r(dup, cut, &ptr);
	if (str == NULL){
		free(dup);
		herror("error : client no input\n");
		return -1;
	}
	int count =0;
	do{
		cmd[count]=strdup(str);
		str = strtok_r(NULL, cut, &ptr);
		count++;
	}while(str);
	cmd[count]=NULL;
	free(dup);
	return count;
}

void readfile(char *filename,char *buff){
	FILE *file=fopen(filename,"rb");
	fseek(file,0,SEEK_END);
	long flen=ftell(file);
	rewind(file);
	fread(buff,1,flen,file);
	fclose(file);
}

int main(int argc,char *argv[]){
	int	sockfd, newsockfd,opt=1,pid;
	socklen_t clilen;
	struct sockaddr_in	cli_addr, serv_addr;
	if((sockfd = socket(AF_INET, SOCK_STREAM, 0))<0)
		herror("server: can't open stream socket");
	bzero((char *) &serv_addr, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
	serv_addr.sin_port = htons(SERV_TCP_PORT);

	char path[1024]={0};
	getcwd(path,1024);

	setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR,&opt, sizeof(opt));
	if(bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr))< 0)
		herror("server: can't bind local address");
	listen(sockfd, 5);
	for(;;){
		char buff[BUFFSIZE]={0};
		char context[BUFFSIZE]={0};
		char *cmd[MAX_CMDNUMBER];
		int count=0;
		clilen = sizeof(cli_addr);
		newsockfd = accept(sockfd, (struct sockaddr *) &cli_addr, &clilen);
		if(newsockfd < 0) herror("server: accept error");

		if((pid=fork())<0) herror("fork error");
		else if(pid==0){
			close(sockfd);
			read(newsockfd,buff,BUFFSIZE);
			count=cutline(buff,cmd," ");
			write(newsockfd,ACK_MSG,strlen(ACK_MSG));
			if(strstr(cmd[1],"?")!=NULL){
				char *ptr,cgi_name[BUFFSIZE]={0};
				ptr=strstr(cmd[1],"?");
				*ptr=0;
				strcpy(cgi_name,cmd[1]);
				setenv("QUERY_STRING",++ptr,1);
				setenv("CONTENT_LENGTH","ANY",1);
				setenv("REQUEST_METHOD","GET",1);
				setenv("SCRIPT_NAME","hw3.cgi",1);
				setenv("REMOTE_HOST","nplinux4",1);
				setenv("REMOTE_ADDR","nplinux4",1);
				setenv("AUTH_TYPE","GUEST",1);
				setenv("REMOTE_USER","ANY",1);
				setenv("REMOTE_IDENT","ANY",1);
				if((pid=fork())<0) herror("fork error");
				else if(pid==0){
					write(newsockfd,CONTENT_TYPE,strlen(CONTENT_TYPE));
					dup2(newsockfd,0);
					dup2(newsockfd,1);
					strcat(path,cgi_name);
					execl(path,cmd[1]+sizeof(char),(char*)0);
					herror("execl fail");
				}
				else wait(0);
			}
			else if(strstr(cmd[1],".")!=NULL){
				if(strcmp(cmd[1],"/form_get.htm")==0){
					readfile("form_get.htm",context);
					write(newsockfd,CONTENT_TYPE,strlen(CONTENT_TYPE));
					write(newsockfd,context,strlen(context));
				}
				else if(strcmp(strstr(cmd[1],"."),".cgi")==0){
					if((pid=fork())<0) herror("fork error");
					else if(pid==0){
					dup2(newsockfd,1);
					strcat(path,cmd[1]);
					execl(path,cmd[1]+sizeof(char),(char*)0);
					herror("execl fail");
					}
					else wait(0);
				}
				else{
					write(newsockfd,CONTENT_TYPE,strlen(CONTENT_TYPE));
					write(newsockfd,"situation2",strlen("situation2"));
				}
			}
			else{
				write(newsockfd,CONTENT_TYPE,strlen(CONTENT_TYPE));
				write(newsockfd,"situation1",strlen("situation1"));
			}
			exit(0);
		}
		else	close(newsockfd);
	}
}
