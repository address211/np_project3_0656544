#include <windows.h>
#include <list>
using namespace std;
#include <stdio.h>
#include "resource.h"
#include <io.h>
#include <fcntl.h>  

#define SERVER_PORT 7799
#define BUFFSIZE 4096
#define WM_SOCKET_NOTIFY (WM_USER + 1)
#define CGI_EXEC (WM_USER + 2)

BOOL CALLBACK MainDlgProc(HWND, UINT, WPARAM, LPARAM);
int EditPrintf (HWND, TCHAR *, ...);
//=================================================================
//	Global Variables
//=================================================================
list<SOCKET> Socks;
struct parsetable {
	char host[1024];
	char port[1024];
	char filename[1024];
	FILE *fd;
} parsetable[5];
char buff[BUFFSIZE] = { 0 }, text[BUFFSIZE] = { 0 }, *cmd[1000];
FILE *file;
SOCKET sockfd[5] = { 0 }, aftersend;
struct sockaddr_in res;
int tag = 0, conn = 0;

int cutline(char *line, char *cmd[], char *cut) {
	if (!line) return 0;
	char *str = NULL;
	char *dup = strdup(line);

	str = strtok(dup, cut);
	if (str == NULL) {
		free(dup);
		return -1;
	}
	int count = 0;
	do {
		cmd[count] = strdup(str);
		str = strtok(NULL, cut);
		count++;
	} while (str);
	cmd[count] = NULL;
	free(dup);
	return count;
}

void send_html(char *str, SOCKET sock) {
	char tmp[5];
	for (int i = 0; i<strlen(str); i++) {
		switch (str[i]) {
		case '>':
			send(sock, "&gt;", 4, 0);
			break;
		case '<':
			send(sock, "&lt;", 4, 0);
			break;
		case '"':
			send(sock, "&quot;", 6, 0);
			break;
		case '\r':
			break;
		case '\n':
			send(sock, "<br>", 4, 0);
			break;
		default:
			sprintf(tmp, "%c", str[i]);
			send(sock, tmp, 1, 0);
			break;
		}
	}
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
	LPSTR lpCmdLine, int nCmdShow)
{
	
	return DialogBox(hInstance, MAKEINTRESOURCE(ID_MAIN), NULL, MainDlgProc);
}

BOOL CALLBACK MainDlgProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam)
{
	WSADATA wsaData;

	static HWND hwndEdit;
	static SOCKET msock, ssock;
	static struct sockaddr_in sa;
	int err;

	switch(Message) 
	{
		case WM_INITDIALOG:
			hwndEdit = GetDlgItem(hwnd, IDC_RESULT);
			break;
		case WM_COMMAND:
			switch(LOWORD(wParam))
			{
				case ID_LISTEN:

					WSAStartup(MAKEWORD(2, 0), &wsaData);

					//create master socket
					msock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

					if( msock == INVALID_SOCKET ) {
						EditPrintf(hwndEdit, TEXT("=== Error: create socket error ===\r\n"));
						WSACleanup();
						return TRUE;
					}

					err = WSAAsyncSelect(msock, hwnd, WM_SOCKET_NOTIFY, FD_ACCEPT | FD_CLOSE | FD_READ | FD_WRITE);

					if ( err == SOCKET_ERROR ) {
						EditPrintf(hwndEdit, TEXT("=== Error: select error ===\r\n"));
						closesocket(msock);
						WSACleanup();
						return TRUE;
					}

					//fill the address info about server
					sa.sin_family		= AF_INET;
					sa.sin_port			= htons(SERVER_PORT);
					sa.sin_addr.s_addr	= INADDR_ANY;

					//bind socket
					err = bind(msock, (LPSOCKADDR)&sa, sizeof(struct sockaddr));

					if( err == SOCKET_ERROR ) {
						EditPrintf(hwndEdit, TEXT("=== Error: binding error ===\r\n"));
						WSACleanup();
						return FALSE;
					}

					err = listen(msock, 2);
		
					if( err == SOCKET_ERROR ) {
						EditPrintf(hwndEdit, TEXT("=== Error: listen error ===\r\n"));
						WSACleanup();
						return FALSE;
					}
					else {
						EditPrintf(hwndEdit, TEXT("=== Server START ===\r\n"));
					}

					break;
				case ID_EXIT:
					EndDialog(hwnd, 0);
					break;
			};
			break;

		case WM_CLOSE:
			EndDialog(hwnd, 0);
			break;

		case WM_SOCKET_NOTIFY:
			switch( WSAGETSELECTEVENT(lParam) )
			{
				case FD_ACCEPT:
					ssock = accept(msock, NULL, NULL);
					Socks.push_back(ssock);
					EditPrintf(hwndEdit, TEXT("=== Accept one new client(%d), List size:%d ===\r\n"), ssock, Socks.size());
					break;
				case FD_READ:
					recv(ssock, buff, BUFFSIZE, 0);
					cutline(buff, cmd, " ");
					sprintf(text, "HTTP/1.1 200 OK\r\nContent-Type: text/html\r\n\r\n");
					send(ssock, text, strlen(text), 0);
					if (strchr(cmd[1], '?') != NULL) {
						aftersend = wParam;
						cutline(cmd[1], cmd, "&");
						sprintf(text, "\
										<html>\r\n\
										<head>\r\n\
										<meta http-equiv=\"Content-Type\" content=\"text/html; charset=big5\" />\r\n\
										<title>Network Programming Homework 3</title>\r\n\
										</head>\r\n\
										<body bgcolor=#336699>\r\n\
										<font face=\"Courier New\" size=2 color=#FFFF99>\r\n\
										<table width=\"800\" border=\"1\">\r\n\
									  ");
						send(ssock, text, strlen(text), 0);
						sprintf(text, "<tr>\r\n");
						send(ssock, text, strlen(text), 0);
						for (int i = 0; i<5; i++) {
							strcpy(parsetable[i].host, strchr(cmd[3 * i], '=') + 1);
							strcpy(parsetable[i].port, strchr(cmd[3 * i + 1], '=') + 1);
							strcpy(parsetable[i].filename, strchr(cmd[3 * i + 2], '=') + 1);
							sprintf(text, "<td>%s</td>\r\n", parsetable[i].host);
							send(ssock, text, strlen(text), 0);
						}
						sprintf(text, "</tr>\r\n");
						send(ssock, text, strlen(text), 0);
						sprintf(text, "<tr>\r\n");
						send(ssock, text, strlen(text), 0);
						for (int i = 0; i<5; i++) {
							sprintf(text, "<td valign=\"top\" id=\"m%d\"></td>", i);
							send(ssock, text, strlen(text), 0);
						}
						sprintf(text, "</tr>\r\n");
						send(ssock, text, strlen(text), 0);
						sprintf(text, "</table>\r\n");
						send(ssock, text, strlen(text), 0);
						for (int i = 0; i<5; i++) {
							if (strlen(parsetable[i].host) != 0) {
								parsetable[i].fd = fopen(parsetable[i].filename, "r");
								sockfd[i] = socket(AF_INET, SOCK_STREAM, 0);
								conn++;
								memset(&res, 0, sizeof(res));
								res.sin_family = AF_INET;
								res.sin_port = htons(atoi(parsetable[i].port));
								res.sin_addr.s_addr = inet_addr(parsetable[i].host);
								WSAAsyncSelect(sockfd[i], hwnd, CGI_EXEC, FD_CLOSE | FD_READ);
								connect(sockfd[i], (struct sockaddr *)&res, sizeof(res));
							}
						}
					}
					else if (strchr(cmd[1], '.') != NULL) {
						if (strcmp(cmd[1], "/form_get.htm") == 0) {
							file = fopen("form_get.htm", "r");
							while (fgets(text, BUFFSIZE, file) != 0) {
								send(ssock, text, strlen(text), 0);								
							}
							closesocket(wParam);
							fclose(file);
						}
						else {
							send(ssock, "there", 5, 0);
							closesocket(wParam);
						}
					}
					else {
						send(ssock, "here", 4, 0);
						closesocket(wParam);
					}
					break;
				case FD_WRITE:
				//Write your code for write event here

					break;
				case FD_CLOSE:
					break;
			};
			break;
		case CGI_EXEC:
			switch (WSAGETSELECTEVENT(lParam))
			{
				case FD_READ:
					for (int i = 0; i < 5; i++) {
						if (sockfd[i] == wParam)
							tag = i;
					}
					memset(buff, 0, BUFFSIZE);					
					recv(wParam, buff, BUFFSIZE,0);
					sprintf(text, "<script>document.all['m%d'].innerHTML += \"", tag);
					send(ssock, text, strlen(text), 0);
					send_html(buff, ssock);
					sprintf(text, "\";</script>");
					send(ssock, text, strlen(text), 0);
					if (strchr(buff, '%') != NULL) {
						fgets(buff, BUFFSIZE, parsetable[tag].fd);
						sprintf(text, "<script>document.all['m%d'].innerHTML += \"<b>", tag);
						send(ssock, text, strlen(text), 0);
						send_html(buff, ssock);
						sprintf(text, "</b>\";</script>");
						send(ssock, text, strlen(text), 0);
						send(wParam, buff, strlen(buff), 0);
					}
					break;
				case FD_CLOSE:
					conn--;
					if (conn == 0)
						closesocket(aftersend);
					break;
			}
			break;
		
		default:
			return FALSE;


	};

	return TRUE;
}

int EditPrintf (HWND hwndEdit, TCHAR * szFormat, ...)
{
     TCHAR   szBuffer [1024] ;
     va_list pArgList ;

     va_start (pArgList, szFormat) ;
     wvsprintf (szBuffer, szFormat, pArgList) ;
     va_end (pArgList) ;

     SendMessage (hwndEdit, EM_SETSEL, (WPARAM) -1, (LPARAM) -1) ;
     SendMessage (hwndEdit, EM_REPLACESEL, FALSE, (LPARAM) szBuffer) ;
     SendMessage (hwndEdit, EM_SCROLLCARET, 0, 0) ;
	 return SendMessage(hwndEdit, EM_GETLINECOUNT, 0, 0); 
}