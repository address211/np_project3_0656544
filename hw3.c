#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#define BUFFSIZE 30000
#define F_CONNECTING 0
#define F_READING 1
#define F_WRITING 2
#define F_DONE 3
struct parsetable{
  char host[1024];
  char port[1024];
  char filename[1024];
  int fd;
} parsetable[5];
int gSc[5]={0};

void caterror(char* p){
	perror(p);
	exit(1);
}

void readfile(char *filename,char *buff){
	FILE *file=fopen(filename,"rb");
	fseek(file,0,SEEK_END);
	long flen=ftell(file);
	rewind(file);
	fread(buff,1,flen,file);
	fclose(file);
}

void print_html(char *str){
  for(int i=0;i<strlen(str);i++){
    switch(str[i]){
      case '>':
      printf("&gt;");
      break;
      case '<':
      printf("&lt;");
      break;
      case '"':
      printf("&quot;");
      break;
      case '\r':
      break;
      case '\n':
      printf("<br>");
      break;
      default:
      printf("%c",str[i]);
      break;
    }
  }
}

int readline(int fd,char *ptr,int maxlen,int i)
{
	int n,rc;
	char c;
	*ptr = 0;
	for(n=1;n<maxlen;n++)
	{
		if((rc=read(fd,&c,1)) == 1)
		{
			*ptr++ = c;
			if(c==' '&& *(ptr-2) =='%'){ gSc[i] = 1; break; }
			if(c=='\n')  break;
		}
		else if(rc==0)
		{
			if(n==1)     return(0);
			else         break;
		}
		else
			return(-1);
	}
	return(n);
}

int cutline(char *line, char *cmd[], char *cut){
	if(!line) return 0;
	char *str = NULL;
	char *ptr = NULL;
	char *dup = strdup(line);

	str = strtok_r(dup, cut, &ptr);
	if (str == NULL){
		free(dup);
		caterror("error : client no input\n");
		return -1;
	}
	int count =0;
	do{
		cmd[count]=strdup(str);
		str = strtok_r(NULL, cut, &ptr);
		count++;
	}while(str);
	cmd[count]=NULL;
	free(dup);
	return count;
}

int main(){
  char *QUERY_STRING=getenv("QUERY_STRING");
  char *cmd[20],buff[BUFFSIZE]={0};
  int conn=0,nfds,len,error;
  memset(parsetable, 0, sizeof(struct parsetable) * 5);
  printf("\
  <html>\r\n\
  <head>\r\n\
	<meta http-equiv=\"Content-Type\" content=\"text/html; charset=big5\" />\r\n\
	<title>Network Programming Homework 3</title>\r\n\
	</head>\r\n\
	<body bgcolor=#336699>\r\n\
	<font face=\"Courier New\" size=2 color=#FFFF99>\r\n\
	<table width=\"800\" border=\"1\">\r\n\
  ");
  cutline(QUERY_STRING,cmd,"&");
  printf("<tr>\r\n");
  for(int i=0;i<5;i++){
    strcpy(parsetable[i].host,strchr(cmd[3*i],'=')+1);
    strcpy(parsetable[i].port,strchr(cmd[3*i+1],'=')+1);
    strcpy(parsetable[i].filename,strchr(cmd[3*i+2],'=')+1);
    printf("<td>%s</td>\r\n",parsetable[i].host);
  }
  printf("</tr>\r\n");
  printf("<tr>\r\n");
  for(int i=0;i<5;i++){
    printf("<td valign=\"top\" id=\"m%d\"></td>",i);
  }
  printf("</tr>\r\n");
  printf("</table>\r\n");
  int sockfd[5]={0},statusA[5]={0},flag;
  struct addrinfo hints,*res;
  fd_set rfds; /* readable file descriptors*/
  fd_set wfds; /* writable file descriptors*/
  fd_set rs; /* active file descriptors*/
  fd_set ws; /* active file descriptors*/
  nfds = FD_SETSIZE;
  FD_ZERO(&rfds);
  FD_ZERO(&wfds);
  FD_ZERO(&rs);
  FD_ZERO(&ws);

  memset(&hints,0,sizeof hints);
  hints.ai_family =AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  for(int i=0;i<5;i++){
    if(strlen(parsetable[i].host)!=0){
      getaddrinfo(parsetable[i].host,parsetable[i].port,&hints,&res);
      if((sockfd[i]=socket(res->ai_family,res->ai_socktype,res->ai_protocol))<0)
      caterror("socket create fail");
      if((connect(sockfd[i],res->ai_addr,res->ai_addrlen))<0)
      caterror("connect fail");
      parsetable[i].fd=open(parsetable[i].filename,O_RDONLY);
      flag = fcntl(sockfd[i], F_GETFL, 0);
      fcntl(sockfd[i], F_SETFL, flag | O_NONBLOCK);
      FD_SET(sockfd[i], &rs);
      FD_SET(sockfd[i], &ws);
      statusA[i] = F_CONNECTING;
      conn++;
    }
    else statusA[i]=F_DONE;
  }
  rfds = rs; wfds = ws;
  while (conn > 0) {
    memcpy(&rfds, &rs, sizeof(rfds));
    memcpy(&wfds, &ws, sizeof(wfds));
    if ( select(nfds, &rfds, &wfds, (fd_set*)0, (struct timeval*)0) < 0 )
      caterror("select error");
    for(int i=0;i<5;i++){
      if(sockfd[i]>0){
        if (statusA[i] == F_CONNECTING && (FD_ISSET(sockfd[i], &rfds) || FD_ISSET(sockfd[i], &wfds))){
          if (getsockopt(sockfd[i], SOL_SOCKET, SO_ERROR, &error, (socklen_t*)&len) < 0 ||error != 0) {
          // non-blocking connect failed
          return (-1);
          }
          statusA[i] = F_READING;
          FD_CLR(sockfd[i], &ws);
        }
        else if (statusA[i] == F_WRITING && FD_ISSET(sockfd[i], &wfds) ) {
          memset(buff,0,BUFFSIZE);
          readline(parsetable[i].fd,buff,BUFFSIZE,i);
          len=write(sockfd[i],buff,strlen(buff));
          printf("<script>document.all['m%d'].innerHTML += \"<b>",i);
          print_html(buff);
          printf("</b>\";</script>");
          // write finished
          FD_CLR(sockfd[i], &ws);
          statusA[i] = F_READING;
          FD_SET(sockfd[i], &rs);
        }
        else if (statusA[i] == F_READING && FD_ISSET(sockfd[i], &rfds) ) {
          memset(buff,0,BUFFSIZE);
          len=readline(sockfd[i],buff,BUFFSIZE,i);
          if (len <= 0) {
          // read finished
          FD_CLR(sockfd[i], &rs);
          statusA[i] = F_DONE ;
          sockfd[i]=0;
          conn--;
          continue;
          }
          printf("<script>document.all['m%d'].innerHTML += \"",i);
          print_html(buff);
          printf("\";</script>");
          fflush(stdout);
          if(gSc[i]==1){
            FD_CLR(sockfd[i], &rs);
            statusA[i] = F_WRITING;
            FD_SET(sockfd[i], &ws);
            gSc[i]=0;
          }
        }
      }
    }
  }
  printf(
  "</font>\r\n\
	</body>\r\n\
	</html>"
  );
}
